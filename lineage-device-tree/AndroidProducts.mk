#
# Copyright (C) 2023 The LineageOS Project
#
# SPDX-License-Identifier: Apache-2.0
#

PRODUCT_MAKEFILES := \
    $(LOCAL_DIR)/lineage_RMP6768.mk

COMMON_LUNCH_CHOICES := \
    lineage_RMP6768-user \
    lineage_RMP6768-userdebug \
    lineage_RMP6768-eng
